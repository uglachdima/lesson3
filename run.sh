#/bin/bash

#docker stop lesson_symfony_union
#docker rm lesson_symfony_union
#docker stop lesson_nginx_union
#docker rm lesson_nginx_union

SYMFONY_INTERNAL_PORT=8000
SYMFONY_EXTERNAL_PORT=8010
NGINX_EXTERNAL_PORT=8011
docker build -t lesson_symfony symfony
docker run -dp $SYMFONY_EXTERNAL_PORT:$SYMFONY_INTERNAL_PORT --name lesson_symfony_union lesson_symfony
SYMFONY_IP=$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' lesson_symfony_union)
echo SYMFONY_IP
docker build --build-arg APP_HOST=$SYMFONY_IP --build-arg APP_PORT=$SYMFONY_INTERNAL_PORT -t lesson_nginx nginx
docker run -dp $NGINX_EXTERNAL_PORT:80 --name lesson_nginx_union lesson_nginx
